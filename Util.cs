using System;
using System.Collections.Generic;
using System.Linq;

namespace MarsRoverChallenge
{
	public static class Util
	{
		public static Direction DirectionFromString(string direction)
		{
			if (direction == "N")
			{
				return Direction.North;
			}
			else if (direction == "E")
			{
				return Direction.East;
			}
			else if (direction == "S")
			{
				return Direction.South;
			}
			else if (direction == "W")
			{
				return Direction.West;
			}

			throw new ArgumentException("Unrecognised direction", nameof(direction));
		}

		public static IEnumerable<Instruction> InstructionsFromString(string instructions)
		{
			return instructions.ToCharArray().Select(c => InstructionFromString(c.ToString()));
		}

		public static Instruction InstructionFromString(string instruction)
		{
			if (instruction == "L")
			{
				return Instruction.Left;
			}
			else if (instruction == "R")
			{
				return Instruction.Right;
			}
			else if (instruction == "M")
			{
				return Instruction.Move;
			}

			throw new ArgumentException("Unrecognised instruction", nameof(instruction));
		}

		public static string ToOrientationString(this Rover rover)
		{
			return $"{rover.X} {rover.Y} {rover.Direction.ToString().Substring(0, 1)}";
		}
	}
}
