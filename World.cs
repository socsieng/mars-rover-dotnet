using System;
using System.Collections.Generic;
using System.Linq;

namespace MarsRoverChallenge
{
	public class World
	{
		private IList<Rover> _rovers;
		public int Width { get; private set; }
		public int Height { get; private set; }

		public World(int width, int height)
		{
			Width = width;
			Height = height;
			_rovers = new List<Rover>();
		}

		internal World(string size) : this(0, 0)
		{
			var dimensions = size.Split(' ').Select(int.Parse);
			Width = dimensions.First();
			Height = dimensions.Last();
		}

		public void Add(Rover rover)
		{
			if (IsLocationAvailable(rover.X, rover.Y))
			{
				rover.Initialize(this);
				_rovers.Add(rover);
			}
			else
			{
				throw new ArgumentException("Position is unavailable", nameof(rover));
			}
		}

		public void SendInstructions(Rover rover, params Instruction[] instructions)
		{
			foreach (var instruction in instructions)
			{
				if (instruction == Instruction.Move)
				{
					// assume that rover can tell if it can move forward, otherwise don't move
					rover.TryMove();
				}
				else if (instruction == Instruction.Left)
				{
					rover.TurnLeft();
				}
				else if (instruction == Instruction.Right)
				{
					rover.TurnRight();
				}
			}
		}

		public bool IsLocationAvailable(int x, int y)
		{
			var isInGrid = x >= 0 && x <= Width && y >= 0 && y <= Height;

			if (isInGrid)
			{
				// check if rover in position
				return !_rovers.Any(r => r.X == x && r.Y == y);
			}

			return false;
		}
	}
}
