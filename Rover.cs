namespace MarsRoverChallenge
{
	public class Rover
	{
		public int X { get; private set; }
		public int Y { get; private set; }
		public Direction Direction { get; private set; }
		private World World { get; set; }

		public Rover(int x, int y, Direction direction)
		{
			X = x;
			Y = y;
			Direction = direction;
		}

		internal Rover(string orientation)
		{
			var components = orientation.Split(' ');
			X = int.Parse(components[0]);
			Y = int.Parse(components[1]);

			var direction = components[2];
			Direction = Util.DirectionFromString(direction);
		}

		public void Initialize(World world)
		{
			World = world;
		}

		/// <summary>
		/// Attempt to move is possible
		///
		/// Assumption here is that the Rover will not attempt to move somewhere it cannot
		/// </summary>
		/// <returns></returns>
		public bool TryMove()
		{
			if (CanMove())
			{
				Move();
				return true;
			}
			return false;
		}

		private bool CanMove()
		{
			if (Direction == Direction.North)
			{
				return World.IsLocationAvailable(X, Y + 1);
			}
			else if (Direction == Direction.East)
			{
				return World.IsLocationAvailable(X + 1, Y);
			}
			else if (Direction == Direction.South)
			{
				return World.IsLocationAvailable(X, Y - 1);
			}
			else
			{
				return World.IsLocationAvailable(X - 1, Y);
			}
		}

		private void Move()
		{
			if (Direction == Direction.North)
			{
				Y++;
			}
			else if (Direction == Direction.East)
			{
				X++;
			}
			else if (Direction == Direction.South)
			{
				Y--;
			}
			else if (Direction == Direction.West)
			{
				X--;
			}
		}

		public void TurnLeft()
		{
			Direction = (Direction)(((int)Direction + 270) % 360);
		}

		public void TurnRight()
		{
			Direction = (Direction)(((int)Direction + 90) % 360);
		}
	}
}
