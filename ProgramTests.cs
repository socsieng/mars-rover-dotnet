using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xunit;

namespace MarsRoverChallenge
{
	public class ProgramTests
	{
		[Fact]
		public void ShouldFailWithEmptyArguments()
		{
			if (Console.IsInputRedirected) return; // skip as it doesn't work properly with Docker

			var result = Program.Main(new string[] {});

			Assert.NotEqual(0, result);
		}

		[Fact]
		public void ShouldFailWithOneArgument()
		{
			var result = Program.Main(new string[] { "file" });

			Assert.NotEqual(0, result);
		}

		[Fact]
		public void ShouldFailWithMoreThanTwoArguments()
		{
			var result = Program.Main(new string[] { "one", "two", "three" });

			Assert.NotEqual(0, result);
		}

		[Fact]
		public void ShouldErrorWhenFileDoesNotExist()
		{
			Assert.Throws<FileNotFoundException>(() => Program.Main(new string[] { "file", "hello.txt" }));
		}

		[Fact]
		public void ShouldErrorWithEmptyContent()
		{
			Assert.Throws<ArgumentException>(() => Program.Main(new string[] { "content", "" }));
		}

		[Fact]
		public void ShouldErrorSingleLine()
		{
			Assert.Throws<ArgumentException>(() => Program.Main(new string[] { "content", "line" }));
		}

		[Fact]
		public void ShouldErrorTwoLines()
		{
			Assert.Throws<ArgumentException>(() => Program.Main(new string[] { "content", "line1\r\nline2" }));
		}

		[Fact]
		public void ShouldErrorTwoLinesTrailingLineBreak()
		{
			Assert.Throws<ArgumentException>(() => Program.Main(new string[] { "content", "line1\r\nline2\r\n" }));
		}

		[Fact]
		public void ShouldErrorWithInvalidFormat()
		{
			Assert.Throws<FormatException>(() => Program.Main(new string[] { "content", "line1\r\nline2\r\nline3" }));
		}

		[Fact]
		public void ShouldErrorWithInvalidContentArguments()
		{
			Assert.Throws<ArgumentException>(() => Program.Main(new string[] { "content", "1\n0 0\n" }));
		}

		[Fact]
		public void ShouldErrorWithInvalidContentType()
		{
			Assert.Throws<ArgumentException>(() => Program.Main(new string[] { "content", "1 1\n0 0 Z\nPPP" }));
		}

		[Fact]
		public void ShouldPassWithValidContent()
		{
			var result = Program.Main(new string[] { "content", "1 1\n0 0 N\nL" });

			Assert.Equal(0, result);
		}

		[Fact]
		public void ShouldPassWithValidContent_MultipleRovers()
		{
			var result = Program.Main(new string[] { "content", "1 1\n0 0 N\nL\n1 1 N\nR" });

			Assert.Equal(0, result);
		}

		[Fact]
		public void ShouldErrorWithValidContent_MultipleRoversInSamePlace()
		{
			Assert.Throws<ArgumentException>(() => Program.Main(new string[] { "content", "1 1\n0 0 N\nL\n0 0 S\nR" }));
		}
	}
}
