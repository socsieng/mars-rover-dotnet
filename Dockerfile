FROM microsoft/dotnet:sdk AS build-env

WORKDIR /app

COPY *.csproj ./
RUN dotnet restore

COPY . ./
RUN dotnet test
RUN dotnet publish -c Release -o out

COPY sample.txt out/

# Build runtime image
FROM microsoft/dotnet:aspnetcore-runtime
WORKDIR /app
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "MarsRoverChallenge.dll", "file", "sample.txt"]
