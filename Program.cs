﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MarsRoverChallenge
{
	class Program
	{
		public static int Main(string[] args)
		{
			string content = null;

			if (args.Length == 2)
			{
				if (args[0] == "file")
				{
					content = File.ReadAllText(args[1]);
				}
				else if (args[0] == "content")
				{
					content = args[1];
				}
				else
				{
					Console.Error.WriteLine($"Unsupported argument: {args[0]}");
					return 1;
				}
			}
			else if (args.Length == 0 && Console.IsInputRedirected)
			{
				using (StreamReader reader = new StreamReader(Console.OpenStandardInput(), Console.InputEncoding))
				{
					content = reader.ReadToEnd();
				}
			}
			else
			{
				Console.Error.WriteLine("Expecting either of the following arguments: file <filename>, or content <content>");
				return 1;
			}

			var lines = content.Split(new [] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries);

			if (lines.Length >= 3 && (lines.Length - 1) % 2 == 0)
			{
				var world = new World(lines[0]);
				var rovers = new List<Rover>();
				var instructions = new List<string>();

				for (var i = 1; i < lines.Length; i++)
				{
					if (i % 2 == 1)
					{
						rovers.Add(new Rover(lines[i]));
					}
					else
					{
						instructions.Add(lines[i]);
					}
				}

				rovers.ForEach(world.Add);

				for (var i = 0; i < rovers.Count; i++)
				{
					var rover = rovers[i];
					world.SendInstructions(rover, Util.InstructionsFromString(instructions[i]).ToArray());
					Console.WriteLine(rover.ToOrientationString());
				}
			}
			else
			{
				throw new ArgumentException("Invalid input", String.Join(", ", args));
			}

			return 0;
		}
	}
}
