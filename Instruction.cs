namespace MarsRoverChallenge
{
	public enum Instruction
	{
		Left,
		Right,
		Move,
	}
}
