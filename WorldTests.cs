using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace MarsRoverChallenge
{
	public class WorldTests
	{
		[Fact]
		public void ShouldAddSingleRover()
		{
			var world = new World(1, 1);
			var rovers = new List<Rover>
			{
				new Rover(0, 0, Direction.North),
			};

			rovers.ForEach(world.Add);
		}

		[Fact]
		public void ShouldAddSingleRoverOnBoundary()
		{
			var world = new World(1, 1);
			var rovers = new List<Rover>
			{
				new Rover(1, 1, Direction.North),
			};

			rovers.ForEach(world.Add);
		}

		[Fact]
		public void ShouldFailToMultipleRoversToSamePosition()
		{
			var world = new World(1, 1);
			var rovers = new List<Rover>
			{
				new Rover(0, 0, Direction.North),
				new Rover(0, 0, Direction.North),
			};

			Assert.Throws<ArgumentException>(() => rovers.ForEach(world.Add));
		}

		[Fact]
		public void ShouldFailAddRoverOutsideAreaX()
		{
			var world = new World(1, 1);
			var rovers = new List<Rover>
			{
				new Rover(2, 0, Direction.North),
			};

			Assert.Throws<ArgumentException>(() => rovers.ForEach(world.Add));
		}

		[Theory]
		[InlineData("0 0", "1 1 N")]
		[InlineData("0 0", "-1 -1 N")]
		[InlineData("-1 -1", "1 1 N")]
		public void ShouldFailAddRoverOutsideAreaY(string grid, string position)
		{
			var world = new World(grid);

			Assert.Throws<ArgumentException>(() => world.Add(new Rover(position)));
		}

		[Theory]
		[InlineData("5 5", "1 2 N", "LMLMLMLMM", "1 3 N")]
		[InlineData("5 5", "3 3 E", "MMRMMRMRRM", "5 1 E")]
		[InlineData("0 0", "0 0 N", "M", "0 0 N")]
		[InlineData("1 1", "0 0 N", "M", "0 1 N")]
		[InlineData("1 1", "0 0 N", "MR", "0 1 E")]
		[InlineData("1 1", "0 0 N", "MRM", "1 1 E")]
		[InlineData("1 1", "0 0 N", "MRMM", "1 1 E")]
		[InlineData("1 1", "0 0 N", "MRMML", "1 1 N")]
		[InlineData("1 1", "0 0 N", "MRMMLL", "1 1 W")]
		[InlineData("1 1", "1 1 N", "MRMMLLM", "0 1 W")]
		public void ShouldMoveRover(string grid, string initial, string instructions, string expected)
		{
			var world = new World(grid);
			var rover = new Rover(initial);

			world.Add(rover);
			world.SendInstructions(rover, Util.InstructionsFromString(instructions).ToArray());

			Assert.Equal(expected, rover.ToOrientationString());
		}
	}
}
